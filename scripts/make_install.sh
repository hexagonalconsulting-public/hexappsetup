cp ./hexappsetup/examples/.gitlab-ci.yml .;
cp ./hexappsetup/examples/.hexappsetup.yml .;
cp ./hexappsetup/examples/app.json .;

mkdir -p .gitlab/issue_templates/;
cp ./hexappsetup/examples/Bug.md .gitlab/issue_templates;
cp ./hexappsetup/examples/Feature.md .gitlab/issue_templates;

app_name="${PWD##*/}";
sed -i '' "s/YourAppName/${app_name}/g" app.json;

if [ -f ~/.rvm/bin/rvm-prompt ]; then
  ruby_version=$(~/.rvm/bin/rvm-prompt $2 $3 $4 $5)
fi
if [ -z "$ruby_version" ]; then
  ruby_version=$(ruby -v | cut -f2 -d' ')
fi

IFS='p' read -ra ruby_version <<< "$ruby_version"
sed -i '' "s/YourRubyVersion/ruby:${ruby_version}/g" .gitlab-ci.yml;
