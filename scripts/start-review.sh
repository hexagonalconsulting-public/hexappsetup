HEROKU_APP_NAME_REVIEW=$1
APP_TARBALL="$HEROKU_APP_NAME_REVIEW.tar.gz"
HEROKU_KEY=$2
PIPELINE_ID=$3

STATUS=$(curl -s -o /dev/null -w '%{http_code}' -n https://api.heroku.com/apps/$HEROKU_APP_NAME_REVIEW \
  -H "Accept: application/vnd.heroku+json; version=3")

if [ $STATUS -eq 404 ]; then
  echo "Fetching env variables from Rails secrets ..."

  VARIABLES_JSON=$(cat .hexappsetup.yml \
    | ruby -ryaml -rjson -e 'puts JSON.pretty_generate(YAML.load(ARGF))' \
    | jq --raw-output '.variables')

  VARIABLES_KEYS=$(echo $VARIABLES_JSON | jq --raw-output 'keys[]')

  for key in ${VARIABLES_KEYS[@]}
  do
    value=$(echo $VARIABLES_JSON | jq --raw-output ".${key}")
    key=$(echo $key | tr '[a-z]' '[A-Z]')
    cat app.json | jq --arg value "$value" '.env = .env + {INEEDTOBEREPLACED:$value}' > tmp-app.json
    sed -i "s/INEEDTOBEREPLACED/${key}/g" tmp-app.json
    mv tmp-app.json app.json
  done

  echo "App setup for $HEROKU_APP_NAME_REVIEW need to be started ..."

  touch $APP_TARBALL
  tar -zcf $APP_TARBALL --exclude=$APP_TARBALL .
  TARBALL_URL=$(curl -s --upload-file $APP_TARBALL "https://transfer.sh/$APP_TARBALL")
  rm $APP_TARBALL

  DOMAIN="$HEROKU_APP_NAME_REVIEW.herokuapp.com"

  STATUS=$(curl -s -o tmp-curl-output.json -w '%{http_code}' -n -X POST https://api.heroku.com/app-setups \
    -H "Content-Type:application/json" \
    -H "Accept:application/vnd.heroku+json; version=3" \
    -H "Authorization: Bearer $HEROKU_KEY" \
    -d '{
      "app": {
        "name": "'"${HEROKU_APP_NAME_REVIEW}"'",
        "region": "eu",
        "stack": "cedar-14"
      },
      "source_blob": {
        "url": "'"${TARBALL_URL}"'"
      },
      "overrides": {
        "env": {
          "HEROKU_APP_NAME": "'"${HEROKU_APP_NAME_REVIEW}"'",
          "DOMAIN": "'"${DOMAIN}"'",
          "REAL_ENV": "review"
        }
      }
    }')

  if [ $STATUS -eq 202 ]; then
    echo "App setup for $HEROKU_APP_NAME_REVIEW correctly started"
    APP_SETUP_ID=$(cat tmp-curl-output.json | jq --raw-output '.id')
    rm tmp-curl-output.json
  else
    echo "Error while trying to start the app setup for $HEROKU_APP_NAME_REVIEW"
    echo "Status $STATUS - https://devcenter.heroku.com/articles/platform-api-reference#statuses"
    cat tmp-curl-output.json && echo '\n'
    rm tmp-curl-output.json
    exit $STATUS
  fi

  echo "App $HEROKU_APP_NAME_REVIEW need to be attached to the project pipeline ..."
  STATUS=$(curl -s -o tmp-curl-output.json -w '%{http_code}' -X POST -H "Accept: application/vnd.heroku+json; version=3" -n \
  -H "Content-Type: application/json" \
  -d '{"app": "'"${HEROKU_APP_NAME_REVIEW}"'", "pipeline": "'"${PIPELINE_ID}"'", "stage": "review"}' \
  -H "Authorization: Bearer $HEROKU_KEY" \
  https://api.heroku.com/pipeline-couplings)

  if [ $STATUS -eq 201 ]; then
    echo "App $HEROKU_APP_NAME_REVIEW correctly attached to the project pipeline"
    rm tmp-curl-output.json
  else
    echo "Error while trying to attache the app $HEROKU_APP_NAME_REVIEW to the project pipeline"
    echo "Status $STATUS - https://devcenter.heroku.com/articles/platform-api-reference#statuses"
    cat tmp-curl-output.json && echo '\n'
    rm tmp-curl-output.json
    exit $STATUS
  fi

  COLLABORATORS_JSON=$(cat .hexappsetup.yml \
    | ruby -ryaml -rjson -e 'puts JSON.pretty_generate(YAML.load(ARGF))' \
    | jq --raw-output '.collaborators[]')

  for collaborator in ${COLLABORATORS_JSON[@]}; do
    echo "Adding $collaborator as collaborator for $HEROKU_APP_NAME_REVIEW ..."
    STATUS=$(curl -s -o tmp-curl-output.json -w '%{http_code}' -n -X POST https://api.heroku.com/apps/$HEROKU_APP_NAME_REVIEW/collaborators \
      -d '{
      "silent": true,
      "user": "'"${collaborator}"'"
    }' \
      -H "Content-Type: application/json" \
      -H "Authorization: Bearer $HEROKU_KEY" \
      -H "Accept: application/vnd.heroku+json; version=3")

    if [ $STATUS -eq 201 ]; then
      echo "$collaborator correctly added as collaborator"
      rm tmp-curl-output.json
    else
      echo "An error occured while trying to add $collaborator as collaborator"
      cat tmp-curl-output.json
      exit $STATUS
    fi
  done

  while $true
  do
    curl -s -o tmp-curl-output.json -n https://api.heroku.com/app-setups/$APP_SETUP_ID \
      -H "Content-Type:application/json" \
      -H "Authorization: Bearer $HEROKU_KEY" \
      -H "Accept:application/vnd.heroku+json; version=3"

    STATUS=$(cat tmp-curl-output.json | jq --raw-output '.status')

    if [ $STATUS == "succeeded" ]; then
      echo "The app setup for $HEROKU_APP_NAME_REVIEW succeeded"
      rm tmp-curl-output.json
      break
    elif [ $STATUS == "failed" ]; then
      echo "The app setup for $HEROKU_APP_NAME_REVIEW failed"
      cat tmp-curl-output.json | jq '.failure_message'
      rm tmp-curl-output.json
      exit 400
    else
      echo "The app setup for $HEROKU_APP_NAME_REVIEW is in $STATUS ... waiting 10 secondes"
    fi
    sleep 10
  done
elif [ $STATUS -eq 200 ]; then
  gem install dpl
  dpl --provider=heroku --app=$HEROKU_APP_NAME_REVIEW --api-key=$HEROKU_KEY
else
  echo "Unexpected error"
  echo "Status $STATUS - https://devcenter.heroku.com/articles/platform-api-reference#statuses"
  exit $STATUS
fi
