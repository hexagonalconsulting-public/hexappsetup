require 'json'

json = JSON.parse(File.read("./tmp-curl-output.json"))

database_url  = json['DATABASE_URL']

regex   = /mysql2:\/\/(.+):(.+)@(.+):(\d+)\/(.+)/
matches = regex.match(database_url)

username  = matches.captures[0]
password  = matches.captures[1]
host      = matches.captures[2]
port      = matches.captures[3]
database  = matches.captures[4]

`mysql -u #{username} -p#{password} -h #{host} -D #{database} < ./db/seed.sql`
