HEROKU_APP_NAME_REVIEW=$1
HEROKU_KEY=$2

STATUS=$(curl -s -o tmp-curl-output.json -w '%{http_code}' -n https://api.heroku.com/apps/$HEROKU_APP_NAME_REVIEW/config-vars \
  -H "Authorization: Bearer $HEROKU_KEY" \
  -H "Accept: application/vnd.heroku+json; version=3")

if [ $STATUS -eq 200 ]; then
  DATABASE_URL=$(cat tmp-curl-output.json | jq --raw-output '.DATABASE_URL')

  if [ $DATABASE_URL != "null" ]; then
    echo "JAWSDB Database already setup"
    rm tmp-curl-output.json
    exit 0
  fi

  echo "Starting setup the JAWSDB MySQL Database"
  sed -i "s/mysql/mysql2/g" tmp-curl-output.json
  DATABASE_URL=$(cat tmp-curl-output.json | jq --raw-output '.JAWSDB_URL')
  STATUS=$(curl -s -o tmp-curl-output.json -w '%{http_code}' -n -X PATCH https://api.heroku.com/apps/$HEROKU_APP_NAME_REVIEW/config-vars \
    -d '{
      "DATABASE_URL": "'"${DATABASE_URL}"'"
    }' \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer $HEROKU_KEY" \
    -H "Accept: application/vnd.heroku+json; version=3")
  if [ $STATUS -eq 200 ]; then
    ruby ./hexappsetup/scripts/db_structure_load.rb
    echo "JAWSDB Database setup done"
    rm tmp-curl-output.json
  else
    echo "Error while trying to setup the MySQL Database"
    echo "Status $STATUS - https://devcenter.heroku.com/articles/platform-api-reference#statuses"
    cat tmp-curl-output.json && echo '\n'
    rm tmp-curl-output.json
    exit $STATUS
  fi
else
  echo "Error while trying to setup the JAWSDB MySQL Database"
  echo "Status $STATUS - https://devcenter.heroku.com/articles/platform-api-reference#statuses"
  cat tmp-curl-output.json && echo '\n'
  rm tmp-curl-output.json
  exit $STATUS
fi
