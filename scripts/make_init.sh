read -p $'\e[35mEnter your Heroku API KEY ( https://dashboard.heroku.com/account ) \e[0m: ' HEROKU_API_KEY
echo ''

echo "\033[36mIs your production application already created ?\033[0m"
select yn in "Yes" "No"; do
  case $yn in
    Yes )
      read -p $'\e[35mEnter the name of your production application \e[0m: ' HEROKU_PROD_APP_NAME

      STATUS=$(curl -s -o .tmphexappsetup.json -w '%{http_code}' -n https://api.heroku.com/apps/$HEROKU_PROD_APP_NAME \
      -H "Authorization: Bearer $HEROKU_API_KEY" \
      -H "Accept: application/vnd.heroku+json; version=3")

      if [ $STATUS -eq 200 ]; then
        STATUS=$(curl -s -o .tmphexappsetup.json -w '%{http_code}' -n https://api.heroku.com/apps/$HEROKU_PROD_APP_NAME/pipeline-couplings \
        -H "Authorization: Bearer $HEROKU_API_KEY" \
        -H "Accept: application/vnd.heroku+json; version=3")

        if [ $STATUS -eq 200 ]; then
          PROD_APP_PIPELINE_ID=$(cat .tmphexappsetup.json | jq --raw-output '.pipeline' | jq --raw-output '.id' )
        fi

        rm .tmphexappsetup.json
      else
        echo "\033[31m\nWe could not find an application for that name\033[0m"
        rm .tmphexappsetup.json
        exit 1
      fi

      break ;;
    No )
      read -p $'\e[35mEnter the name you want for your production application \e[0m: ' HEROKU_PROD_APP_NAME

      STATUS=$(curl -s -o .tmphexappsetup.json -w '%{http_code}' -n -X POST https://api.heroku.com/apps \
      -d '{
        "name": "'"${HEROKU_PROD_APP_NAME}"'",
        "region": "eu",
        "stack": "cedar-14"
      }' \
      -H "Content-Type: application/json" \
      -H "Authorization: Bearer $HEROKU_API_KEY" \
      -H "Accept: application/vnd.heroku+json; version=3")

      if [ $STATUS -eq 201 ]; then
        echo "\033[33m\nApp $HEROKU_PROD_APP_NAME correctly created\033[0m"
        rm .tmphexappsetup.json
      else
        echo "\033[31m\nError while trying to create the app $HEROKU_PROD_APP_NAME"
        echo "Status $STATUS - https://devcenter.heroku.com/articles/platform-api-reference#statuses\033[0m"
        cat .tmphexappsetup.json
        rm .tmphexappsetup.json
        exit 1
      fi

      break ;;
  esac
done
echo ''

echo "\033[36mIs your preproduction application already created ?\033[0m"
select yn in "Yes" "No"; do
  case $yn in
    Yes )
      read -p $'\e[35mEnter the name of your preproduction application \e[0m: ' HEROKU_PREPROD_APP_NAME

      STATUS=$(curl -s -o .tmphexappsetup.json -w '%{http_code}' -n https://api.heroku.com/apps/$HEROKU_PREPROD_APP_NAME \
      -H "Authorization: Bearer $HEROKU_API_KEY" \
      -H "Accept: application/vnd.heroku+json; version=3")

      if [ $STATUS -eq 200 ]; then
        STATUS=$(curl -s -o .tmphexappsetup.json -w '%{http_code}' -n https://api.heroku.com/apps/$HEROKU_PREPROD_APP_NAME/pipeline-couplings \
        -H "Authorization: Bearer $HEROKU_API_KEY" \
        -H "Accept: application/vnd.heroku+json; version=3")

        if [ $STATUS -eq 200 ]; then
          PREPROD_APP_PIPELINE_ID=$(cat .tmphexappsetup.json | jq --raw-output '.pipeline' | jq --raw-output '.id' )
        fi

        rm .tmphexappsetup.json
      else
        echo "\033[31mWe could not find an application for that name\033[0m"
        rm .tmphexappsetup.json
        exit 1
      fi

      break ;;
    No )
      read -p $'\e[35mEnter the name you want for your preproduction application \e[0m: ' HEROKU_PREPROD_APP_NAME

      STATUS=$(curl -s -o .tmphexappsetup.json -w '%{http_code}' -n -X POST https://api.heroku.com/apps \
      -d '{
        "name": "'"${HEROKU_PREPROD_APP_NAME}"'",
        "region": "eu",
        "stack": "cedar-14"
      }' \
      -H "Content-Type: application/json" \
      -H "Authorization: Bearer $HEROKU_API_KEY" \
      -H "Accept: application/vnd.heroku+json; version=3")

      if [ $STATUS -eq 201 ]; then
        echo "\033[33m\nApp $HEROKU_PREPROD_APP_NAME correctly created\033[0m"
        rm .tmphexappsetup.json
      else
        echo "\033[31m\nError while trying to create the app $HEROKU_PREPROD_APP_NAME"
        echo "Status $STATUS - https://devcenter.heroku.com/articles/platform-api-reference#statuses\033[0m"
        cat .tmphexappsetup.json
        rm .tmphexappsetup.json
        exit 1
      fi

      break ;;
  esac
done

attache_app_to_pipeline () {
  APP_NAME=$1
  PIPELINE_ID=$2
  STAGE=$3

  echo "\n\033[33mApp $APP_NAME need to be attached to the project pipeline ...\033[0m"

  STATUS=$(curl -s -o tmp-curl-output.json -w '%{http_code}' -X POST -H "Accept: application/vnd.heroku+json; version=3" -n \
  -H "Content-Type: application/json" \
  -d '{"app": "'"${APP_NAME}"'", "pipeline": "'"${PIPELINE_ID}"'", "stage": "'"${STAGE}"'"}' \
  -H "Authorization: Bearer $HEROKU_API_KEY" \
  https://api.heroku.com/pipeline-couplings)

  if [ $STATUS -eq 201 ]; then
    echo "\033[33mApp $APP_NAME correctly attached to the project pipeline\033[0m"
    rm tmp-curl-output.json
  else
    echo "\033[31mError while trying to attache the app $APP_NAME to the project pipeline"
    echo "Status $STATUS - https://devcenter.heroku.com/articles/platform-api-reference#statuses\033[0m"
    cat tmp-curl-output.json
    rm tmp-curl-output.json
    exit 1
  fi
}

if [ -z $PROD_APP_PIPELINE_ID ] && [ -z $PREPROD_APP_PIPELINE_ID ]; then
  STATUS=$(curl -s -o .tmphexappsetup.json -w '%{http_code}' -n -X POST https://api.heroku.com/pipelines \
  -d '{
    "name": "'"${HEROKU_PROD_APP_NAME}"'"
  }' \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $HEROKU_API_KEY" \
  -H "Accept: application/vnd.heroku+json; version=3")

  HEROKU_PIPELINE_ID=$(cat .tmphexappsetup.json | jq --raw-output '.id')
  rm .tmphexappsetup.json

  attache_app_to_pipeline $HEROKU_PROD_APP_NAME $HEROKU_PIPELINE_ID 'production'
  attache_app_to_pipeline $HEROKU_PREPROD_APP_NAME $HEROKU_PIPELINE_ID 'staging'
elif ! [ -z $PROD_APP_PIPELINE_ID ] && [ -z $PREPROD_APP_PIPELINE_ID ]; then
  HEROKU_PIPELINE_ID=$PROD_APP_PIPELINE_ID

  attache_app_to_pipeline $HEROKU_PREPROD_APP_NAME $HEROKU_PIPELINE_ID 'staging'
elif [ -z $PROD_APP_PIPELINE_ID ] && ! [ -z $PREPROD_APP_PIPELINE_ID ]; then
  HEROKU_PIPELINE_ID=$PREPROD_APP_PIPELINE_ID

  attache_app_to_pipeline $HEROKU_PROD_APP_NAME $HEROKU_PIPELINE_ID 'production'
elif ! [ $PROD_APP_PIPELINE_ID = $PREPROD_APP_PIPELINE_ID ]; then
  HEROKU_PIPELINE_ID=$PROD_APP_PIPELINE_ID

  attache_app_to_pipeline $HEROKU_PREPROD_APP_NAME $HEROKU_PIPELINE_ID 'staging'
fi

echo "\n\033[33mYour pipeline is ready to go ( https://dashboard.heroku.com/pipelines/$HEROKU_PIPELINE_ID )\033[0m"

echo "\n\033[33mNow you need to go to your GitLab project settings to add some environments variables (Settings > CI/CD Pipelines)\033[0m"

echo "\n"
echo "\tHEROKU_APP_NAME_PREPROD   = $HEROKU_PREPROD_APP_NAME"
echo "\tHEROKU_KEY                = $HEROKU_API_KEY"
echo "\tPIPELINE_ID               = $PIPELINE_ID"
echo "\tRSPEC_POSTGRES_DB         = $HEROKU_PROD_APP_NAME-test"
echo "\tSSH_PRIVATE_KEY           = check ./hexappsetup/examples/gitlab_rsa"
echo "\n"
