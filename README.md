# What is Hexappsetup

**Hexappsetup** is a git submodules which allow you to easily setup a new project on `GitLab` and `Heroku` using:

1. GitLab CI
2. GitLab review apps
3. Heroku Pipeline

# How to use Hexappsetup

The first step is to install **Hexappsetup** then just use the Makefile to initialize and install all you need

## Install Hexappsetup

1. `git clone git@gitlab.com:hexagonalconsulting-public/hexappsetup.git`
2. `cp ./hexappsetup/examples/Makefile .`

## Use Hexappsetup

1. `brew install jq`
2. `make init`
3. `make install`
4. `rm -rf ./hexappsetup Makefile`
